# Weather Forecast

---
## Project

For development, you will need Node.js (10.15.3+) and a node global package, NPM, installed in your environment.

### Development Framework

- React
- Redux
- Redux Thunk
- React Bootstrap

### Testing Framework

- Jest
- enzyme

### External API

- [OpenWeatherAPI](https://openweathermap.org/api)

### Development Prerequisite

2. Create `.env` file & put it in root directory. It is the OpenWeatherAPI API Key.

  ```
    REACT_APP_WEATHER_API_KEY=
  ```

### Quick Start

      $  npm install
      $  npm run start

### Test

      $ npm run test

### Additional Information

  - Project used the idea of [ATOM Design](http://atomicdesign.bradfrost.com/chapter-2/)