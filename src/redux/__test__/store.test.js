import createReduxStore, { RootReducer } from '../store';
import { createStore } from 'redux';

describe('redux store', () => {
  it('RootReducer', () => {
    const store = createStore(RootReducer)
    expect(store.getState().weatherData.data).toEqual({});
    expect(store.getState().weatherData.error).toEqual(false);
    expect(store.getState().weatherData.city).toEqual("");
    expect(store.getState().weatherData.loading).toEqual(false);
  });

  it('createStore', () => {
    const store = createReduxStore();
    expect(store.getState().weatherData.data).toEqual({});
    expect(store.getState().weatherData.error).toEqual(false);
    expect(store.getState().weatherData.city).toEqual("");
    expect(store.getState().weatherData.loading).toEqual(false);
  });
});
