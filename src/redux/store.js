import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";

import { weatherDataReducer } from "./weatherData/reducer";

export const RootReducer = combineReducers({
  weatherData: weatherDataReducer
});

const createReduxStore = () => createStore(RootReducer, applyMiddleware(thunk));

export default createReduxStore;
