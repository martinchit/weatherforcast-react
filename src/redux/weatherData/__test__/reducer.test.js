import { weatherDataReducer, initialState } from '../reducer';
import { GET_WEATHER_DATA_REQUEST, GET_WEATHER_DATA_SUCCESS, GET_WEATHER_DATA_ERROR } from '../actions';

describe('WeatherData reducer.js', () => {
  it('should render initial state', () => {
    expect(weatherDataReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle GET_WEATHER_DATA_REQUEST', () => {
    const city = 'test'
    const action = { type: GET_WEATHER_DATA_REQUEST, city };
    expect(weatherDataReducer(undefined, action)).toEqual({ ...initialState, loading: true, city, data: {} });
  });

  it('should handle GET_WEATHER_DATA_SUCCESS', () => {
    const data = {}
    const action = { type: GET_WEATHER_DATA_SUCCESS, data };
    expect(weatherDataReducer(undefined, action)).toEqual({ ...initialState, loading: false, error: false, data });
  });

  it('should handle GET_WEATHER_DATA_ERROR', () => {
    const action = { type: GET_WEATHER_DATA_ERROR };
    expect(weatherDataReducer(undefined, action)).toEqual({ ...initialState, loading: false, error: true, city: 'Search Again'});
  });
});
