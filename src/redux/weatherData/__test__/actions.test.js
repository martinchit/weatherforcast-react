import mockAxios from 'axios';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {
  GET_WEATHER_DATA_REQUEST,
  GET_WEATHER_DATA_SUCCESS,
  GET_WEATHER_DATA_ERROR,
  getWeatherDataRequest,
  getWeatherDataSuccess,
  getWeatherDataError,
  getWeatherData
} from '../actions';
jest.mock('../../store.js')

describe('weatherData action.js', () => {
  const waitForAsync = () => new Promise(resolve => setImmediate(resolve))
  const middleware = [thunk];
  const mockStore = configureStore(middleware);
  const store = mockStore({});

  it('triggers GET_WEATHER_DATA_REQUEST', () => {
    const city = 'test'
    const expectedAction = {
      type: GET_WEATHER_DATA_REQUEST,
    };
    expect(getWeatherDataRequest(city)).toEqual({ ...expectedAction, city });
  });

  it('triggers GET_WEATHER_DATA_SUCCESS', () => {
    const data = {}
    const expectedAction = {
      type: GET_WEATHER_DATA_SUCCESS,
    };
    expect(getWeatherDataSuccess(data)).toEqual({ ...expectedAction, data });
  });
  it('triggers GET_WEATHER_DATA_ERROR', () => {
    const expectedAction = {
      type: GET_WEATHER_DATA_ERROR,
    };
    expect(getWeatherDataError()).toEqual(expectedAction);
  });

  it('runs getWeatherData', async (done) => {
    mockAxios.mockImplementationOnce(() => {
      return Promise.resolve({
        status: 200, 
        data: {
          list: [
            {
              "dt": 1585288800,
              "main": {
                "temp":26.32,
                "feels_like":28.22,
                "temp_min":25.06,
                "temp_max":26.32,
                "pressure":1012,
                "sea_level":1012,
                "grnd_level":1012,
                "humidity":76,
                "temp_kf":1.26
              },
              "weather": [
                {
                  "id":800,
                  "main":"Clear",
                  "description":"clear sky",
                  "icon":"01d"
                }
              ],
              "clouds": { "all":2 },
              "wind":{ "speed":3.81, "deg":165 },
              "sys":{"pod":"d"},
              "dt_txt": "2020-03-27 06:00:00"
            },
            {
              "dt": 1285288800,
              "main": {
                "temp":26.32,
                "feels_like":28.22,
                "temp_min":25.06,
                "temp_max":26.32,
                "pressure":1012,
                "sea_level":1012,
                "grnd_level":1012,
                "humidity":76,
                "temp_kf":1.26
              },
              "weather": [
                {
                  "id":800,
                  "main":"Clear",
                  "description":"clear sky",
                  "icon":"01d"
                }
              ],
              "clouds": { "all":2 },
              "wind":{ "speed":3.81, "deg":165 },
              "sys":{"pod":"d"},
              "dt_txt": "2020-03-27 06:00:00"
            }
          ]
        } 
      })
    });
    const expectedActions = [ 
      { type: 'GET_WEATHER_DATA_REQUEST', city: 'Hong Kong' },
      { type: "GET_WEATHER_DATA_SUCCESS",
        data : { 
          "27/03/2020 Friday" : [
            {
               "day": "Friday",
               "feelsLike": "28.22°C",
               "grndLevel": 1012,
               "humidity": "76%",
               "icon": "http://openweathermap.org/img/w/01d.png",
               "id": 1585288800,
               "pressure": 1012,
               "seaLevel": 1012,
               "temp": "26.32",
               "tempKF": 1.26,
               "tempMax": "26.32°C",
               "tempMin": "25.06°C",
               "timestamp": "27/03/2020 14:00:00",
               "weatherDescription": "clear sky",
               "weatherMain": "Clear",
               "windDirection": "SSE",
               "windSpeed": "3.8mps",
            }
          ],
          "24/09/2010 Friday": [
            {
              "day": "Friday",
              "feelsLike": "28.22°C",
              "grndLevel": 1012,
              "humidity": "76%",
              "icon": "http://openweathermap.org/img/w/01d.png",
              "id": 1285288800,
              "pressure": 1012,
              "seaLevel": 1012,
              "temp": "26.32",
              "tempKF": 1.26,
              "tempMax": "26.32°C",
              "tempMin": "25.06°C",
              "timestamp": "24/09/2010 08:40:00",
              "weatherDescription": "clear sky",
              "weatherMain": "Clear",
              "windDirection": "SSE",
              "windSpeed": "3.8mps",
            },
          ],
        }
      }
    ]
    store.dispatch(getWeatherData('hong kong'))
    await waitForAsync();
    expect(mockAxios).toHaveBeenCalled()
    expect(store.getActions()).toEqual(expectedActions)
    done();
  })

  it('dispatch error in getWeatherData', async (done) => {
    store.clearActions()
    mockAxios.mockImplementationOnce(() => Promise.reject({}) );
    const expectedActions = [
      { type: 'GET_WEATHER_DATA_REQUEST', city: 'Hong Kong' },
      { type: 'GET_WEATHER_DATA_ERROR' }
    ]
    store.dispatch(getWeatherData('hong kong'))
    await waitForAsync();
    expect(store.getActions()).toEqual(expectedActions)
    expect(mockAxios).toHaveBeenCalled()
    done();
  })
});
