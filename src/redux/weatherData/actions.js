import moment from "moment";
import { WeatherInstance } from "../../core/utilities/axiosInstances";
import { degreeToCompass } from "../../core/utilities/degreeToCompass";

export const GET_WEATHER_DATA_REQUEST = "GET_WEATHER_DATA_REQUEST";
export const GET_WEATHER_DATA_SUCCESS = "GET_WEATHER_DATA_SUCCESS";
export const GET_WEATHER_DATA_ERROR = "GET_WEATHER_DATA_ERROR";

export const getWeatherDataRequest = city => ({
  type: GET_WEATHER_DATA_REQUEST,
  city
});

export const getWeatherDataSuccess = data => ({
  type: GET_WEATHER_DATA_SUCCESS,
  data
});

export const getWeatherDataError = () => ({
  type: GET_WEATHER_DATA_ERROR
});

export const getWeatherData = city => dispatch => {
  const cityShowingValue = city
    .split(" ")
    .map(e => `${e[0].toUpperCase()}${e.slice(1)}`)
    .join(" ");
  const cityApiRequestValue = city.toLowerCase().replace(/\s/g, "");
  dispatch(getWeatherDataRequest(cityShowingValue));
  WeatherInstance({
    method: "get",
    params: {
      q: cityApiRequestValue,
      appid: process.env.REACT_APP_WEATHER_API_KEY,
      units: "metric"
    }
  })
    .then(res => {
      const { data } = res;
      const { list } = data;
      const processData = list.reduce((a, e) => {
        const { dt, main, weather, wind } = e;
        const localTimestamp = moment
          .utc(dt * 1000)
          .local()
          .format("DD/MM/YYYY HH:mm:ss dddd");
        const obj = {
          id: dt,
          temp: `${main["temp"]}`,
          feelsLike: `${main["feels_like"]}°C`,
          tempMin: `${main["temp_min"]}°C`,
          tempMax: `${main["temp_max"]}°C`,
          pressure: main["pressure"],
          seaLevel: main["sea_level"],
          grndLevel: main["grnd_level"],
          humidity: `${main["humidity"]}%`,
          tempKF: main["temp_kf"],
          weatherMain: weather[0]["main"],
          weatherDescription: weather[0]["description"],
          icon: `http://openweathermap.org/img/w/${weather[0]["icon"]}.png`,
          windSpeed: `${wind["speed"].toFixed(1)}mps`,
          windDirection: degreeToCompass(wind["deg"]),
          timestamp: localTimestamp.slice(0, 19),
          day: localTimestamp.slice(20)
        };
        const date = localTimestamp
          .slice(0, 11)
          .concat(localTimestamp.slice(20));
        a[date] = a[date] ? a[date].concat(obj) : [].concat(obj);
        return a;
      }, {});
      dispatch(getWeatherDataSuccess(processData));
    })
    .catch(() => {
      dispatch(getWeatherDataError());
    });
};
