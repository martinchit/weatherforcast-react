import { updateObject } from "../../core/utilities/updateObject";
import {
  GET_WEATHER_DATA_REQUEST,
  GET_WEATHER_DATA_SUCCESS,
  GET_WEATHER_DATA_ERROR
} from "./actions";

export const initialState = {
  city: "",
  data: {},
  error: false,
  loading: false
};

const getWeatherDataRequest = (state, action) =>
  updateObject(state, { loading: true, city: action["city"], data: {} });

const getWeatherDataSuccess = (state, action) =>
  updateObject(state, {
    loading: false,
    error: false,
    data: { ...action["data"] }
  });

const getWeatherDataError = state =>
  updateObject(state, { loading: false, error: true, city: "Search Again" });

export const weatherDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_WEATHER_DATA_REQUEST:
      return getWeatherDataRequest(state, action);
    case GET_WEATHER_DATA_SUCCESS:
      return getWeatherDataSuccess(state, action);
    case GET_WEATHER_DATA_ERROR:
      return getWeatherDataError(state);
    default:
      return state;
  }
};
