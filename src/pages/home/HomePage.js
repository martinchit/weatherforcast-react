import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { HeaderBox } from "./component/organisms/HeaderBox";
import { SearchBox } from "./component/organisms/SearchBox";
import { WeatherInfoBox } from "./component/organisms/WeatherInfoBox";
import { FlexBox } from "../../core/component/atoms/FlexBox";
import { Spinner } from "../../core/component/molecules/Spinner";
import { ErrorAlert } from "../../core/component/molecules/ErrorAlert";
import { getWeatherData } from "../../redux/weatherData/actions";

export class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: "",
      expandDate: null
    };
  }

  componentDidMount = () => {
    this.retrieveWeatherData();
  };

  retrieveWeatherData = (city = "Hong Kong") => {
    const { getWeatherDataAction } = this.props;
    getWeatherDataAction(city);
  };

  searchValueOnChangeHandler = searchValue => this.setState({ searchValue });

  searchValueOnClickHandler = () => {
    const { searchValue } = this.state;
    if (searchValue) {
      const trimmed = searchValue.trim().replace(/\d/gi, "");
      this.setState({ searchValue: "" }, () => {
        if (trimmed) {
          this.retrieveWeatherData(trimmed);
        }
      });
    }
  };

  moreButtonOnClick = i => {
    this.setState({ expandDate: i });
  };

  modalOnHideHandler = () => {
    this.setState({ expandDate: null });
  };

  render() {
    const { expandDate, searchValue } = this.state;
    const { city, error, loading, weatherData } = this.props;
    return (
      <FlexBox>
        <FlexBox flexDirection="column">
          <HeaderBox title={city} />
          <SearchBox
            searchValue={searchValue}
            searchValueOnChange={this.searchValueOnChangeHandler}
            searchButtonOnClick={this.searchValueOnClickHandler}
          />
          <FlexBox>
            {loading ? (
              <Spinner />
            ) : error ? (
              <ErrorAlert errorMessage="Incorrect City Value" />
            ) : (
              <WeatherInfoBox
                weatherData={weatherData}
                expandDate={expandDate}
                moreButtonOnClick={this.moreButtonOnClick}
                onHideHandler={this.modalOnHideHandler}
                city={city}
              />
            )}
          </FlexBox>
        </FlexBox>
      </FlexBox>
    );
  }
}

HomePage.propTypes = {
  getWeatherDataAction: PropTypes.func,
  city: PropTypes.string,
  error: PropTypes.bool,
  loading: PropTypes.bool,
  weatherData: PropTypes.shape({
    date: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        temp: PropTypes.string,
        feelsLike: PropTypes.string,
        tempMin: PropTypes.string,
        tempMax: PropTypes.string,
        pressure: PropTypes.number,
        seaLevel: PropTypes.number,
        grndLevel: PropTypes.number,
        humidity: PropTypes.string,
        tempKF: PropTypes.number,
        weatherMain: PropTypes.string,
        weatherDescription: PropTypes.string,
        icon: PropTypes.string,
        windSpeed: PropTypes.string,
        windDirection: PropTypes.string,
        timestamp: PropTypes.string,
        day: PropTypes.string
      })
    )
  })
};

HomePage.defaultProps = {
  getWeatherDataAction: null,
  city: "",
  error: false,
  loading: false,
  weatherData: {}
};

const mapStateToProps = state => ({
  city: state.weatherData.city,
  weatherData: state.weatherData.data,
  error: state.weatherData.error,
  loading: state.weatherData.loading
});

const mapDispatchToProps = dispatch => ({
  getWeatherDataAction: city => dispatch(getWeatherData(city))
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
