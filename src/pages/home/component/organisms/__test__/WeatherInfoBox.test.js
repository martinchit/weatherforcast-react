import React from "react"
import { shallow } from "enzyme";

import { WeatherInfoBox } from "../WeatherInfoBox"
import { WeatherSummaryCard } from "../../molecules/WeatherSummaryCard";
import { WeatherDetailsModal } from "../../molecules/WeatherDetailsModal";

describe("WeatherInfoBox", () => {
  const props = {
    city: "Hong Kong",
    expandDate: "01/04/2020 Wednesday",
    moreButtonOnClick: jest.fn(),
    onHideHandler: jest.fn(),
    weatherData: {
      "01/04/2020 Wednesday" : [
        {
          id: 1585677600,
          temp: "23.2",
          feelsLike: "25.53°C",
          tempMin: "23.2°C",
          tempMax: "23.2°C",
          pressure: 1014,
          seaLevel: 1014,
          grndLevel: 1014,
          humidity: "88%",
          tempKF: 0,
          weatherMain: "Clouds",
          weatherDescription: "few clouds",
          icon: "http://openweathermap.org/img/w/02n.png",
          windSpeed: "2.7mps",
          windDirection: "ESE",
          timestamp: "01/04/2020 02:00:00",
          day: "Wednesday"
        }
      ]
    }
  }
  it("renders correctly", () => {
    const wrapper = shallow(<WeatherInfoBox />)
    expect(wrapper).toMatchSnapshot()
  })

  it("renders correct weather information", () => {
    const wrapper = shallow(<WeatherInfoBox {...props} />)
    expect(wrapper.find(WeatherDetailsModal)).toHaveLength(1)
    expect(wrapper.find(WeatherSummaryCard)).toHaveLength(1)
    wrapper.find(WeatherSummaryCard).first().props().moreButtonOnClick()
    expect(props["moreButtonOnClick"]).toHaveBeenCalled()
    expect(props["moreButtonOnClick"]).toHaveBeenCalledWith("01/04/2020 Wednesday")
  })
})
