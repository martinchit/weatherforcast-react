import React from "react"
import { shallow } from 'enzyme';

import { SearchBox } from "../SearchBox"
import { SearchField } from '../../atoms/SearchField';
import { SearchButton } from '../../atoms/SearchButton';

describe("SearchBox", () => {
  const props = {
    searchValueOnChange: jest.fn(),
    searchButtonOnClick: jest.fn(),
    searchValue: '',
  }
  it("renders correctly", () => {
    const wrapper = shallow(<SearchBox />)
    expect(wrapper).toMatchSnapshot()
  })

  it("handle search value on change", () => {
    const event = {
      preventDefault() {},
      target: { value: 'change '}
    };
    const wrapper = shallow(<SearchBox {...props} />)
    wrapper.find(SearchField).first().simulate('change', event);
    expect(props.searchValueOnChange).toHaveBeenCalled();
    expect(props.searchValueOnChange).toHaveBeenCalledWith(event.target.value);
  })

  it("handle search action", () => {
    const wrapper = shallow(<SearchBox {...props} />)
    wrapper.find(SearchButton).first().simulate('click');
    expect(props.searchButtonOnClick).toHaveBeenCalled();
    jest.clearAllMocks();
  })

  it("handle search action when key is ENTER", () => {
    const event = {
      preventDefault() {},
      keyCode: 13
    };
    const wrapper = shallow(<SearchBox {...props} />)
    wrapper.find(SearchField).first().simulate('keydown', event);
    expect(props.searchButtonOnClick).toHaveBeenCalled();
    jest.clearAllMocks();
  })

  it("does not handle search action when key is not ENTER", () => {
    const event = {
      preventDefault() {},
      keyCode: 37
    };
    const wrapper = shallow(<SearchBox {...props} />)
    wrapper.find(SearchField).first().simulate('keydown', event);
    expect(props.searchButtonOnClick).not.toHaveBeenCalled();
    jest.clearAllMocks();
  })
})
