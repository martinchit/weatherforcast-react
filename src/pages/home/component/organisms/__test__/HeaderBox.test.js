import React from "react"
import { shallow } from 'enzyme';

import { HeaderBox } from "../HeaderBox"

describe("HeaderBox", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<HeaderBox />)
    expect(wrapper).toMatchSnapshot()
  })
})
