import React from "react";
import PropTypes from "prop-types";
import { WeatherSummaryCard } from "../molecules/WeatherSummaryCard";
import { WeatherDetailsModal } from "../molecules/WeatherDetailsModal";
import { FlexBox } from "../../../../core/component/atoms/FlexBox";

export const WeatherInfoBox = ({
  city,
  weatherData,
  expandDate,
  moreButtonOnClick,
  onHideHandler
}) => (
  <FlexBox flexDirection="column">
    {expandDate && (
      <WeatherDetailsModal
        weatherData={weatherData[expandDate]}
        timestamp={expandDate.split(" ")}
        show={!!expandDate}
        onHideHandler={onHideHandler}
        city={city}
      />
    )}
    <FlexBox justifyContent="space-around">
      {Object.entries(weatherData).map(([timestamp, data]) => (
        <WeatherSummaryCard
          {...data[0]}
          key={data[0]["id"]}
          moreButtonOnClick={() => moreButtonOnClick(timestamp)}
        />
      ))}
    </FlexBox>
  </FlexBox>
);

WeatherInfoBox.propTypes = {
  city: PropTypes.string,
  expandDate: PropTypes.string,
  moreButtonOnClick: PropTypes.func,
  onHideHandler: PropTypes.func,
  weatherData: PropTypes.shape({
    date: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        temp: PropTypes.string,
        feelsLike: PropTypes.string,
        tempMin: PropTypes.string,
        tempMax: PropTypes.string,
        pressure: PropTypes.number,
        seaLevel: PropTypes.number,
        grndLevel: PropTypes.number,
        humidity: PropTypes.string,
        tempKF: PropTypes.number,
        weatherMain: PropTypes.string,
        weatherDescription: PropTypes.string,
        icon: PropTypes.string,
        windSpeed: PropTypes.string,
        windDirection: PropTypes.string,
        timestamp: PropTypes.string,
        day: PropTypes.string
      })
    )
  })
};

WeatherInfoBox.defaultProps = {
  city: "",
  expandDate: "",
  moreButtonOnClick: null,
  onHideHandler: null,
  weatherData: {}
};
