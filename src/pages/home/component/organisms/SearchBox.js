import React from "react";
import PropTypes from "prop-types";
import { FlexBox } from "../../../../core/component/atoms/FlexBox";
import { SearchField } from "../atoms/SearchField";
import { SearchButton } from "../atoms/SearchButton";

export const SearchBox = ({
  searchValue,
  searchValueOnChange,
  searchButtonOnClick
}) => (
  <FlexBox width="60%" justifyContent="space-around" flexDirection="column">
    <SearchField
      type="text"
      placeholder="Search By City Name"
      onChange={e => searchValueOnChange(e.target.value)}
      value={searchValue}
      onKeyDown={e => (e.keyCode === 13 ? searchButtonOnClick() : null)}
    />
    <SearchButton
      variant="primary"
      onClick={searchButtonOnClick}
      disabled={!searchValue.length}
    >
      Search
    </SearchButton>
  </FlexBox>
);

SearchBox.propTypes = {
  searchValueOnChange: PropTypes.func,
  searchButtonOnClick: PropTypes.func,
  searchValue: PropTypes.string
};

SearchBox.defaultProps = {
  searchValueOnChange: null,
  searchButtonOnClick: null,
  searchValue: ""
};
