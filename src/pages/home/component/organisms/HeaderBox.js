import React from "react";
import PropTypes from "prop-types";
import { Header } from "../atoms/Header";
import { FlexBox } from "../../../../core/component/atoms/FlexBox";

export const HeaderBox = ({ title }) => (
  <FlexBox margin="1rem 0px">
    <Header title={title} />
  </FlexBox>
);

HeaderBox.propTypes = {
  title: PropTypes.string
};

HeaderBox.defaultProps = {
  title: ""
};
