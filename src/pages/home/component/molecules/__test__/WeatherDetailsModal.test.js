import React from "react"
import { shallow } from "enzyme";
import { Button } from "react-bootstrap";

import { WeatherDetailsModal } from "../WeatherDetailsModal"
import { WeatherDataIcon } from "../../atoms/WeatherDataIcon";
import { WeatherDetailsModalContainer } from "../../atoms/WeatherDetailsModalContainer";
import { WeatherModalCardContainer } from "../../atoms/WeatherModalCardContainer";
import { WeatherDataKey } from "../../atoms/WeatherDataKey";
import { WeatherDataTitle } from "../../atoms/WeatherDataTitle";
import { WeatherDataSubTitle } from "../../atoms/WeatherDataSubTitle";
import { WeatherDataContainer } from "../../atoms/WeatherDataContainer";
import { WeatherDataValue } from "../../atoms/WeatherDataValue";

describe("WeatherDetailsModal", () => {
  const props = {
    city: "Hong Kong",
    show: true,
    onHideHandler: jest.fn(),
    timestamp: ["01/04/2020", "Wednesday"],
    weatherData: [
      {
        id: 1585677600,
        temp: "23.2",
        feelsLike: "25.53°C",
        tempMin: "23.2°C",
        tempMax: "23.2°C",
        pressure: 1014,
        seaLevel: 1014,
        grndLevel: 1014,
        humidity: "88%",
        tempKF: 0,
        weatherMain: "Clouds",
        weatherDescription: "few clouds",
        icon: "http://openweathermap.org/img/w/02n.png",
        windSpeed: "2.7mps",
        windDirection: "ESE",
        timestamp: "01/04/2020 02:00:00",
        day: "Wednesday"
      }
    ]
  }
  it("renders correctly", () => {
    const wrapper = shallow(<WeatherDetailsModal />)
    expect(wrapper).toMatchSnapshot()
  })

  it("renders weather information", () => {
    const processedTitle = props["weatherData"][0]["timestamp"].slice(11, 16)
    const processedSubtitle = props["weatherData"][0]["weatherDescription"].split(" ").map(e => `${e[0].toUpperCase()}${e.slice(1)}`).join(" ")
    const wrapper = shallow(<WeatherDetailsModal {...props} />)
    expect(wrapper.find(WeatherDetailsModalContainer)).toHaveLength(1)
    expect(wrapper.find(WeatherModalCardContainer)).toHaveLength(1)
    expect(wrapper.find(WeatherDataIcon).first().props().src).toEqual(props["weatherData"][0]["icon"])
    expect(wrapper.find(WeatherDataTitle).first().props().title).toEqual(processedTitle)
    expect(wrapper.find(WeatherDataSubTitle).first().props().subtitle).toEqual(processedSubtitle)
    expect(wrapper.find(WeatherDataKey)).toHaveLength(5)
    expect(wrapper.find(WeatherDataValue)).toHaveLength(5)
    expect(wrapper.find(WeatherDataContainer)).toHaveLength(5)
    wrapper.find(Button).first().simulate("click");
    expect(props["onHideHandler"]).toHaveBeenCalled()
  })
})
