import React from "react"
import { shallow } from 'enzyme';

import { WeatherSummaryCard } from "../WeatherSummaryCard"

describe("WeatherSummaryCard", () => {
  const props = {
    temp: '',
    tempMin: '',
    tempMax: '',
    humidity: '',
    weatherDescription: 'test',
    icon: '',
    windSpeed: '',
    windDirection: '',
    timestamp: '',
    day: '',
    moreButtonOnClick: null,
  }
  it("renders correctly", () => {
    const wrapper = shallow(<WeatherSummaryCard {...props} />)
    expect(wrapper).toMatchSnapshot()
  })
})
