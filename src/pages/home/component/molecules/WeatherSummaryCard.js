import React from "react";
import PropTypes from "prop-types";
import { WeatherDataIcon } from "../atoms/WeatherDataIcon";
import { WeatherSummaryCardContainer } from "../atoms/WeatherSummaryCardContainer";
import { WeatherDataTitle } from "../atoms/WeatherDataTitle";
import { WeatherDataSubTitle } from "../atoms/WeatherDataSubTitle";
import { WeatherDataContainer } from "../atoms/WeatherDataContainer";
import { WeatherDataKey } from "../atoms/WeatherDataKey";
import { WeatherDataValue } from "../atoms/WeatherDataValue";
import { MoreButton } from "../atoms/MoreButton";

export const WeatherSummaryCard = ({
  icon,
  day,
  timestamp,
  tempMax,
  tempMin,
  weatherDescription,
  windSpeed,
  windDirection,
  moreButtonOnClick
}) => (
  <WeatherSummaryCardContainer flexDirection="column">
    <WeatherDataIcon src={icon} />
    <WeatherDataTitle title={timestamp.slice(0, 11)} />
    <WeatherDataTitle title={timestamp.slice(11, 16)} />
    <WeatherDataSubTitle
      subtitle={weatherDescription
        .split(" ")
        .map(e => `${e[0].toUpperCase()}${e.slice(1)}`)
        .join(" ")}
    />
    <WeatherDataContainer>
      <WeatherDataKey title="Day" />
      <WeatherDataValue value={day} />
    </WeatherDataContainer>
    <WeatherDataContainer>
      <WeatherDataKey title="Max Temp" />
      <WeatherDataValue value={tempMax} />
    </WeatherDataContainer>
    <WeatherDataContainer>
      <WeatherDataKey title="Min Temp" />
      <WeatherDataValue value={tempMin} />
    </WeatherDataContainer>
    <WeatherDataContainer>
      <WeatherDataKey title="Wind" />
      <WeatherDataValue value={`${windSpeed} ${windDirection}`} />
    </WeatherDataContainer>
    <MoreButton variant="info" onClick={moreButtonOnClick}>
      More
    </MoreButton>
  </WeatherSummaryCardContainer>
);

WeatherSummaryCard.propTypes = {
  temp: PropTypes.string,
  tempMin: PropTypes.string,
  tempMax: PropTypes.string,
  humidity: PropTypes.string,
  weatherDescription: PropTypes.string,
  icon: PropTypes.string,
  windSpeed: PropTypes.string,
  windDirection: PropTypes.string,
  timestamp: PropTypes.string,
  day: PropTypes.string,
  moreButtonOnClick: PropTypes.func
};

WeatherSummaryCard.defaultProps = {
  temp: "",
  tempMin: "",
  tempMax: "",
  humidity: "",
  weatherDescription: "",
  icon: "",
  windSpeed: "",
  windDirection: "",
  timestamp: "",
  day: "",
  moreButtonOnClick: null
};
