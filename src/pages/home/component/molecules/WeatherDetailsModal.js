import React from "react";
import { Button, Modal } from "react-bootstrap";
import PropTypes from "prop-types";
import { WeatherDataIcon } from "../atoms/WeatherDataIcon";
import { WeatherDetailsModalContainer } from "../atoms/WeatherDetailsModalContainer";
import { WeatherModalCardContainer } from "../atoms/WeatherModalCardContainer";
import { WeatherDataKey } from "../atoms/WeatherDataKey";
import { WeatherDataTitle } from "../atoms/WeatherDataTitle";
import { WeatherDataSubTitle } from "../atoms/WeatherDataSubTitle";
import { WeatherDataContainer } from "../atoms/WeatherDataContainer";
import { WeatherDataValue } from "../atoms/WeatherDataValue";

export const WeatherDetailsModal = ({
  weatherData,
  show,
  onHideHandler,
  timestamp,
  city
}) => (
  <Modal show={show} centered onHide={onHideHandler}>
    <Modal.Header closeButton>
      <Modal.Title>{`${city} - ${timestamp[0]} (${timestamp[1]})`}</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <WeatherDetailsModalContainer>
        {weatherData.map(
          ({
            id,
            icon,
            day,
            timestamp,
            tempMax,
            tempMin,
            weatherDescription,
            windSpeed,
            windDirection,
            humidity
          }) => (
            <WeatherModalCardContainer key={id}>
              <WeatherDataIcon src={icon} />
              <WeatherDataTitle title={timestamp.slice(11, 16)} />
              <WeatherDataSubTitle
                subtitle={weatherDescription
                  .split(" ")
                  .map(e => `${e[0].toUpperCase()}${e.slice(1)}`)
                  .join(" ")}
              />
              <WeatherDataContainer>
                <WeatherDataKey title="Day" />
                <WeatherDataValue value={day} />
              </WeatherDataContainer>
              <WeatherDataContainer>
                <WeatherDataKey title="Max Temp" />
                <WeatherDataValue value={tempMax} />
              </WeatherDataContainer>
              <WeatherDataContainer>
                <WeatherDataKey title="Min Temp" />
                <WeatherDataValue value={tempMin} />
              </WeatherDataContainer>
              <WeatherDataContainer>
                <WeatherDataKey title="Wind" />
                <WeatherDataValue value={`${windSpeed} ${windDirection}`} />
              </WeatherDataContainer>
              <WeatherDataContainer>
                <WeatherDataKey title="Humidity" />
                <WeatherDataValue value={humidity} />
              </WeatherDataContainer>
            </WeatherModalCardContainer>
          )
        )}
      </WeatherDetailsModalContainer>
    </Modal.Body>
    <Modal.Footer>
      <Button variant="secondary" onClick={onHideHandler}>
        Close
      </Button>
    </Modal.Footer>
  </Modal>
);

WeatherDetailsModal.propTypes = {
  city: PropTypes.string,
  show: PropTypes.bool,
  onHideHandler: PropTypes.func,
  timestamp: PropTypes.array,
  weatherData: PropTypes.arrayOf(
    PropTypes.shape({
      date: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number,
          temp: PropTypes.string,
          feelsLike: PropTypes.string,
          tempMin: PropTypes.string,
          tempMax: PropTypes.string,
          pressure: PropTypes.number,
          seaLevel: PropTypes.number,
          grndLevel: PropTypes.number,
          humidity: PropTypes.string,
          tempKF: PropTypes.number,
          weatherMain: PropTypes.string,
          weatherDescription: PropTypes.string,
          icon: PropTypes.string,
          windSpeed: PropTypes.string,
          windDirection: PropTypes.string,
          timestamp: PropTypes.string,
          day: PropTypes.string
        })
      )
    })
  )
};

WeatherDetailsModal.defaultProps = {
  city: "",
  show: false,
  onHideHandler: null,
  timestamp: [],
  weatherData: []
};
