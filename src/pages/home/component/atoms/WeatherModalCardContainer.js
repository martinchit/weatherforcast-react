import styled from "styled-components";

export const WeatherModalCardContainer = styled.div`
  width: 12%;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (max-width: 1700px) {
    width: 25%;
    border: 0;
    display: inline-block;
    text-align: center;
  }
  @media (max-width: 767px) {
    width: 50%;
    border: 0;
    display: inline-block;
    text-align: center;
  }
`;
