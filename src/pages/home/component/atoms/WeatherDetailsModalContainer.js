import styled from "styled-components";

export const WeatherDetailsModalContainer = styled.div`
  justify-content: space-between;
  width: 100%;
  align-items: center;
  margin: 0;
  padding: 20px 5px;
  flex-wrap: wrap;
  display: flex;

  @media (max-width: 1700px) {
    display: inline-block;
    white-space: nowrap;
    overflow-x: scroll;
    text-align: center;
  }
`;
