import styled from "styled-components";
import { Button } from "react-bootstrap";

export const MoreButton = styled(Button)`
  width: auto;
  margin-top: 20px;
  padding: 8px 30px;

  @media (max-width: 767px) {
    margin-top: 12px;
  }
`;
