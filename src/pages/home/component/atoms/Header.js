import React from "react";
import PropTypes from "prop-types";
import { Text } from "../../../../core/component/atoms/Text";

export const Header = ({ title }) => (
  <Text fontSize="3x" fontWeight="bold">
    {title}
  </Text>
);

Header.propTypes = {
  title: PropTypes.string
};

Header.defaultProps = {
  title: ""
};
