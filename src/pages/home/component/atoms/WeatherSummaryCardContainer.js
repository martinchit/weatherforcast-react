import styled from "styled-components";
import { FlexBox } from "../../../../core/component/atoms/FlexBox";

export const WeatherSummaryCardContainer = styled(FlexBox)`
  background-color: #fff;
  box-shadow: 0 2.8px 2.2px rgba(0, 0, 0, 0.034),
    0 6.7px 5.3px rgba(0, 0, 0, 0.048), 0 12.5px 10px rgba(0, 0, 0, 0.06),
    0 22.3px 17.9px rgba(0, 0, 0, 0.072), 0 41.8px 33.4px rgba(0, 0, 0, 0.086),
    0 100px 80px rgba(0, 0, 0, 0.12);
  width: 15%;
  padding: 20px 10px;
  border-radius: 10px;
  margin: 20px 0px;

  @media (max-width: 1300px) {
    width: 80%;
    padding: 20px 10%;
  }
`;
