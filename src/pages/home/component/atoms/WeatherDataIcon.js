import styled from "styled-components";

export const WeatherDataIcon = styled.img`
  height: 6vw;
  width: 6vw;

  @media (max-width: 1200px) {
    height: 15vw;
    width: 15vw;
  }
`;
