import React from "react";
import PropTypes from "prop-types";
import { Text } from "../../../../core/component/atoms/Text";

export const WeatherDataTitle = ({ title }) => (
  <Text fontWeight="bold" fontSize="1x">
    {title}
  </Text>
);

WeatherDataTitle.propTypes = {
  title: PropTypes.string
};

WeatherDataTitle.defaultProps = {
  title: ""
};
