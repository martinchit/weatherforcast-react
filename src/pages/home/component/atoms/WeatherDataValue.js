import React from "react";
import PropTypes from "prop-types";
import { Text } from "../../../../core/component/atoms/Text";

export const WeatherDataValue = ({ value }) => (
  <Text fontColor="#000" fontWeight="bold" fontSize="1x">
    {value}
  </Text>
);

WeatherDataValue.propTypes = {
  value: PropTypes.string
};

WeatherDataValue.defaultProps = {
  value: ""
};
