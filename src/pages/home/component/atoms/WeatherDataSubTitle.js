import React from "react";
import PropTypes from "prop-types";
import { Text } from "../../../../core/component/atoms/Text";

export const WeatherDataSubTitle = ({ subtitle }) => (
  <Text fontWeight="bold" fontSize="1x">
    {subtitle}
  </Text>
);

WeatherDataSubTitle.propTypes = {
  subtitle: PropTypes.string
};

WeatherDataSubTitle.defaultProps = {
  subtitle: ""
};
