import styled from "styled-components";
import { Button } from "react-bootstrap";

export const SearchButton = styled(Button)`
  width: 40%;
  margin: 20px;

  @media (max-width: 767px) {
    margin: 12px;
  }
`;
