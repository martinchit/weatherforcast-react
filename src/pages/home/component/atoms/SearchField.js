import styled from "styled-components";
import { Form } from "react-bootstrap";

export const SearchField = styled(Form.Control)`
  width: 80%;

  @media (max-width: 767px) {
    width: 100%;
  }
`;
