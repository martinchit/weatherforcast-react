import styled from "styled-components";
import { FlexBox } from "../../../../core/component/atoms/FlexBox";

export const WeatherDataContainer = styled(FlexBox)`
  justify-content: space-between;
  align-items: flex-start";
  width: 90%;
  margin: 6px 0px;
`;
