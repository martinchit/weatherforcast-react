import React from "react"
import { shallow } from 'enzyme';

import { WeatherDataTitle } from "../WeatherDataTitle"

describe("WeatherDataTitle", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<WeatherDataTitle />)
    expect(wrapper).toMatchSnapshot()
  })
})
