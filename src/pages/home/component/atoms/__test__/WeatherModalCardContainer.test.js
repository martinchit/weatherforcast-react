import React from "react"
import { shallow } from 'enzyme';

import { WeatherModalCardContainer } from "../WeatherModalCardContainer"

describe("WeatherModalCardContainer", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<WeatherModalCardContainer />)
    expect(wrapper).toMatchSnapshot()
  })
})
