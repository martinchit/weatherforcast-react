import React from "react"
import { shallow } from 'enzyme';

import { WeatherDataIcon } from "../WeatherDataIcon"

describe("WeatherDataIcon", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<WeatherDataIcon />)
    expect(wrapper).toMatchSnapshot()
  })
})
