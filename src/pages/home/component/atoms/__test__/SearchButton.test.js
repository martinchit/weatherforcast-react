import React from "react"
import { shallow } from 'enzyme';

import { SearchButton } from "../SearchButton"

describe("SearchButton", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<SearchButton />)
    expect(wrapper).toMatchSnapshot()
  })
})
