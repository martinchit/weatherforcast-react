import React from "react"
import { shallow } from 'enzyme';

import { WeatherDataContainer } from "../WeatherDataContainer"

describe("WeatherDataContainer", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<WeatherDataContainer />)
    expect(wrapper).toMatchSnapshot()
  })
})
