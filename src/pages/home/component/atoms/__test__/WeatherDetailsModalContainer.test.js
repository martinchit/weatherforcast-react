import React from "react"
import { shallow } from 'enzyme';

import { WeatherDetailsModalContainer } from "../WeatherDetailsModalContainer"

describe("WeatherDetailsModalContainer", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<WeatherDetailsModalContainer />)
    expect(wrapper).toMatchSnapshot()
  })
})
