import React from "react"
import { shallow } from 'enzyme';

import { WeatherDataKey } from "../WeatherDataKey"

describe("WeatherDataKey", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<WeatherDataKey />)
    expect(wrapper).toMatchSnapshot()
  })
})
