import React from "react"
import { shallow } from 'enzyme';

import { MoreButton } from "../MoreButton"

describe("MoreButton", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<MoreButton />)
    expect(wrapper).toMatchSnapshot()
  })
})
