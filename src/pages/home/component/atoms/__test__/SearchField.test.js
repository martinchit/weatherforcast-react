import React from "react"
import { shallow } from 'enzyme';

import { SearchField } from "../SearchField"

describe("SearchField", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<SearchField />)
    expect(wrapper).toMatchSnapshot()
  })
})
