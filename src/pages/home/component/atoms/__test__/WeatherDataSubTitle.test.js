import React from "react"
import { shallow } from 'enzyme';

import { WeatherDataSubTitle } from "../WeatherDataSubTitle"

describe("WeatherDataSubTitle", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<WeatherDataSubTitle />)
    expect(wrapper).toMatchSnapshot()
  })
})
