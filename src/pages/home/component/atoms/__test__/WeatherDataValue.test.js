import React from "react"
import { shallow } from 'enzyme';

import { WeatherDataValue } from "../WeatherDataValue"

describe("WeatherDataValue", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<WeatherDataValue />)
    expect(wrapper).toMatchSnapshot()
  })
})
