import React from "react"
import { shallow } from 'enzyme';

import { WeatherSummaryCardContainer } from "../WeatherSummaryCardContainer"

describe("WeatherSummaryCardContainer", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<WeatherSummaryCardContainer />)
    expect(wrapper).toMatchSnapshot()
  })
})
