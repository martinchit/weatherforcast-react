import React from "react";
import PropTypes from "prop-types";
import { Text } from "../../../../core/component/atoms/Text";

export const WeatherDataKey = ({ title }) => (
  <Text fontColor="#aaa" fontWeight="bold" fontSize="1x">
    {title}
  </Text>
);

WeatherDataKey.propTypes = {
  title: PropTypes.string
};

WeatherDataKey.defaultProps = {
  title: ""
};
