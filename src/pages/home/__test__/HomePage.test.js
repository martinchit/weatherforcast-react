import React from "react"
import { shallow } from 'enzyme';

import { HomePage, mapDispatchToProps } from "../HomePage"
import { SearchBox } from '../component/organisms/SearchBox';
import { WeatherInfoBox } from '../component/organisms/WeatherInfoBox';
import { Spinner } from '../../../core/component/molecules/Spinner';
import { ErrorAlert } from '../../../core/component/molecules/ErrorAlert';
import { getWeatherData } from '../../../redux/weatherData/actions';

describe("HomePage", () => {
  const props = {
    getWeatherDataAction: jest.fn(),
    weatherData: {
      "01/04/2020 Wednesday" : [
        {
          id: 1585677600,
          temp: "23.2",
          feelsLike: "25.53°C",
          tempMin: "23.2°C",
          tempMax: "23.2°C",
          pressure: 1014,
          seaLevel: 1014,
          grndLevel: 1014,
          humidity: "88%",
          tempKF: 0,
          weatherMain: "Clouds",
          weatherDescription: "few clouds",
          icon: "http://openweathermap.org/img/w/02n.png",
          windSpeed: "2.7mps",
          windDirection: "ESE",
          timestamp: "01/04/2020 02:00:00",
          day: "Wednesday"
        }
      ]
    },
    loading: false,
    error: false,
  }
  it("renders correctly", () => {
    const wrapper = shallow(<HomePage {...props} />)
    expect(wrapper).toMatchSnapshot()
  })

  it("renders loading Spinner", () => {
    const loadingScenario = { ...props, loading: true }
    const wrapper = shallow(<HomePage {...loadingScenario} />)
    expect(props.getWeatherDataAction).toHaveBeenCalled()
     expect(props.getWeatherDataAction).toHaveBeenCalledWith('Hong Kong')
    expect(wrapper.find(Spinner)).toHaveLength(1)
    expect(wrapper.find(ErrorAlert)).toHaveLength(0)
    expect(wrapper.find(WeatherInfoBox)).toHaveLength(0)
    jest.clearAllMocks()
  })

  it("renders loading ErrorAlert", () => {
    const errorScenario = { ...props, error: true }
    const wrapper = shallow(<HomePage {...errorScenario} />)
    expect(props.getWeatherDataAction).toHaveBeenCalled()
    expect(props.getWeatherDataAction).toHaveBeenCalledWith('Hong Kong')
    expect(wrapper.find(Spinner)).toHaveLength(0)
    expect(wrapper.find(ErrorAlert)).toHaveLength(1)
    expect(wrapper.find(WeatherInfoBox)).toHaveLength(0)
    jest.clearAllMocks()
  })

  it("changes expandDate when moreButtonOnClick", () => {
    const wrapper = shallow(<HomePage {...props} />)
    wrapper.find(WeatherInfoBox).first().props().moreButtonOnClick('test')
    expect(wrapper.state().expandDate).toEqual('test')
  })

  it("changes expandDate when modalOnHideHandler", () => {
    const wrapper = shallow(<HomePage {...props} />)
    wrapper.find(WeatherInfoBox).first().props().onHideHandler()
    expect(wrapper.state().expandDate).toEqual(null)
  })

  it("changes searchValue when searchValueOnChange", () => {
    const wrapper = shallow(<HomePage {...props} />)
    wrapper.find(SearchBox).first().props().searchValueOnChange('test')
    expect(wrapper.state().searchValue).toEqual('test')
  })

  it("changes handle searchValueOnClickHandler", () => {
    const wrapper = shallow(<HomePage {...props} />)
    wrapper.setState({ searchValue: 'test' })
    wrapper.find(SearchBox).first().props().searchButtonOnClick()
    expect(wrapper.state().searchValue).toEqual('')
    expect(props.getWeatherDataAction).toHaveBeenCalledWith('test')
  })
})
