import React from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from "react-router-dom";
import HomePage from "./pages/home/HomePage";
import { Header } from "./core/component/organisms/Header";

const App = () => (
  <Router>
    <Header />
    <Switch>
      <Route path="/">
        <HomePage />
      </Route>
      <Redirect from="*" to="/" />
    </Switch>
  </Router>
);

export default App;
