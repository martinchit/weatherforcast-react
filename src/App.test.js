import React from "react";
import { shallow } from 'enzyme';
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";

import App from './App';

const mockStore = configureMockStore();
const mockWeatherReducer = {
  city: "",
  data: {},
  error: false,
  loading: false,
}
const store = mockStore({
  weatherData : mockWeatherReducer
});

describe("App", () => {
  it("renders correctly", () => {
    const wrapper = shallow(
      <Provider store={store}>
        <App />
      </Provider>
    );
    expect(wrapper).toMatchSnapshot()
  });
});