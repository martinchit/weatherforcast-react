import React from "react";
import PropTypes from "prop-types";
import { FlexBox } from "../atoms/FlexBox";
import { Text } from "../atoms/Text";

export const ErrorAlert = ({ errorMessage }) => (
  <FlexBox flexDirection="column">
    <Text fontWeight="extra-bold" fontSize="2x" fontColor="red">
      Error
    </Text>
    <Text
      fontWeight="extra-bold"
      fontSize="1x"
      style={{ textAlign: "center" }}
      fontColor="red"
    >
      {errorMessage}
    </Text>
  </FlexBox>
);

ErrorAlert.propTypes = {
  errorMessage: PropTypes.string
};

ErrorAlert.defaultProps = {
  errorMessage: ""
};
