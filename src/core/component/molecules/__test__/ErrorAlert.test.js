import React from "react"
import { shallow } from 'enzyme';

import { ErrorAlert } from "../ErrorAlert"

describe("ErrorAlert", () => {
  it("renders correctly", () => {
    const wrapper = shallow(<ErrorAlert />)
    expect(wrapper).toMatchSnapshot()
  })
})
