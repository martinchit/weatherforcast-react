import React from "react"
import { Navbar } from 'react-bootstrap';
import { shallow } from 'enzyme';
import { BrowserRouter as Router } from 'react-router-dom';

import { Header } from "../Header"

describe("Header", () => {
  it("renders correctly", () => {
    const wrapper = shallow(
      <Router>
        <Header />
      </Router>
    )
    expect(wrapper).toMatchSnapshot()
  })
})
