import React from "react";
import { Link } from "react-router-dom";
import { Navbar } from "react-bootstrap";
import { Text } from "../atoms/Text";

export const Header = () => (
  <Navbar bg="dark" expand="lg">
    <Navbar.Brand as={Link} to="/">
      <Text fontSize="2x" fontColor="#fff" fontWeight="bold">
        Weather Forecast
      </Text>
    </Navbar.Brand>
  </Navbar>
);
