import { degreeToCompass } from '../degreeToCompass';

describe('degreeToCompass function', () => {
  it('renders correct direction N', () => {
    const direction = degreeToCompass(360);
    expect(direction).toBe('N');
  });

  it('renders correct direction NNW', () => {
    const direction = degreeToCompass(330);
    expect(direction).toBe('NNW');
  });

  it('renders correct direction NW', () => {
    const direction = degreeToCompass(320);
    expect(direction).toBe('NW');
  });

  it('renders correct direction WNW', () => {
    const direction = degreeToCompass(300);
    expect(direction).toBe('WNW');
  });

  it('renders correct direction W', () => {
    const direction = degreeToCompass(280);
    expect(direction).toBe('W');
  });

  it('renders correct direction WSW', () => {
    const direction = degreeToCompass(240);
    expect(direction).toBe('WSW');
  });

  it('renders correct direction SW', () => {
    const direction = degreeToCompass(220);
    expect(direction).toBe('SW');
  });

  it('renders correct direction SSW', () => {
    const direction = degreeToCompass(200);
    expect(direction).toBe('SSW');
  });

  it('renders correct direction S', () => {
    const direction = degreeToCompass(180);
    expect(direction).toBe('S');
  });

  it('renders correct direction SSE', () => {
    const direction = degreeToCompass(160);
    expect(direction).toBe('SSE');
  });

  it('renders correct direction SE', () => {
    const direction = degreeToCompass(140);
    expect(direction).toBe('SE');
  });

  it('renders correct direction ESE', () => {
    const direction = degreeToCompass(120);
    expect(direction).toBe('ESE');
  });

  it('renders correct direction E', () => {
    const direction = degreeToCompass(100);
    expect(direction).toBe('E');
  });

  it('renders correct direction ENE', () => {
    const direction = degreeToCompass(60);
    expect(direction).toBe('ENE');
  });

  it('renders correct direction NE', () => {
    const direction = degreeToCompass(40);
    expect(direction).toBe('NE');
  });

  it('renders correct direction NNE', () => {
    const direction = degreeToCompass(20);
    expect(direction).toBe('NNE');
  });
});
