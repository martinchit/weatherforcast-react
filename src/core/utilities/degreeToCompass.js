export const degreeToCompass = deg => {
  const directions = [
    "N",
    "NNE",
    "NE",
    "ENE",
    "E",
    "ESE",
    "SE",
    "SSE",
    "S",
    "SSW",
    "SW",
    "WSW",
    "W",
    "WNW",
    "NW",
    "NNW"
  ];
  const val = Math.floor(deg / 22.5 + 0.5) % 16;
  return directions[val];
};
